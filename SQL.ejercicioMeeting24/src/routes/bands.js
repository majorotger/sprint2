const express = require('express')
const router = express.Router()
const { sequelize } = require("../connection/sequelize")

router.get("/", async (req, res) => {
    const bands = await sequelize.query("SELECT * FROM bands", {type: sequelize.QueryTypes.SELECT})
    res.json({bands})
})

router.get("/:id", async (req, res) => {
    const idBand = parseInt(req.params.id)
    try {
            const bands = await sequelize.query(`SELECT bands.name as band_name, albums.album_name, albums.album_publication_date FROM bands INNER JOIN albums ON bands.id = albums.band WHERE bands.id = ${idBand}`, {type: sequelize.QueryTypes.SELECT})
            if (bands.length === 0) {
                res.status(400).json({"message": "The band does not exist"})
            } else{
                res.status(200).json({bands});    
            }
    } catch (error) {
        res.status(400).json({"error": error.message})
    }
})

router.post("/", async (req, res) => {
    const {name, members, start_date, separation_date, country} = req.body
    const band = await sequelize.query(`INSERT INTO bands (name, members, start_date, separation_date, country) VALUES ("${name}","${members}","${start_date}","${separation_date}","${country}")`, {type: sequelize.QueryTypes.INSERT})
    res.json({band})
})

router.put("/:id", async (req, res) => {
    const {name, members, start_date, separation_date, country} = req.body
    const idBand = parseInt(req.params.id)
    try{
        const band = await sequelize.query(`UPDATE bands SET name = "${name}", members = "${members}", start_date = "${start_date}", separation_date = "${separation_date}", country = "${country}" WHERE id = ${idBand}`, {type: sequelize.QueryTypes.UPDATE})
        res.json({band})
    }catch (error){
        console.log(error.message)
        res.status(400).json({"error": error.message})
    }
})

router.delete("/:id", async (req, res) => {
    const idBand = parseInt(req.params.id)
    try{
        const band = await sequelize.query(`DELETE FROM bands WHERE id = ${idBand}`, {type: sequelize.QueryTypes.DELETE})
        res.json({"message": "banda eliminada"})
    }catch (error){
        console.log(error.message)
        res.status(400).json({"error": error.message})
    }
})


module.exports = router