const express = require('express')
const router = express.Router()
const { sequelize } = require("../connection/sequelize")

router.get("/", async (req, res) => {
    const albums = await sequelize.query("SELECT * FROM albums", {type: sequelize.QueryTypes.SELECT})
    res.json({albums})
})


router.get("/:id", async (req, res) => {
    const idAlbum = parseInt(req.params.id)
    try {
        const albums = await sequelize.query(`SELECT albums.*, songs.song_name, bands.name as band_name FROM albums INNER JOIN songs ON albums.id = songs.album INNER JOIN bands ON albums.band = bands.id WHERE albums.id = ${idAlbum}`, {type: sequelize.QueryTypes.SELECT})
        if (albums.length === 0) {
            res.status(400).json({"message": "The album does not exist"})
        } else{
            res.status(200).json({albums});    
        }
    } catch (error) {
        res.status(400).json({"error": error.message})
    }
    
})

router.post("/", async (req, res) => {
    const {album_name, band, album_publication_date} = req.body
    const album = await sequelize.query(`INSERT INTO albums (album_name, band, album_publication_date) VALUES ("${album_name}","${band}","${album_publication_date}")`, {type: sequelize.QueryTypes.INSERT})
    res.json({album})
})

router.put("/:id", async (req, res) => {
    const {album_name, band, album_publication_date} = req.body
    const idAlbum = parseInt(req.params.id)
    try{
        const album = await sequelize.query(`UPDATE albums SET album_name = "${album_name}", band = "${band}", album_publication_date = "${album_publication_date}" WHERE id = ${idAlbum}`, {type: sequelize.QueryTypes.UPDATE})
        res.json({album})
    }catch (error){
        console.log(error.message)
        res.status(400).json({"error": error.message})
    }
})

router.delete("/:id", async (req, res) => {
    const idAlbum = parseInt(req.params.id)
    try{
        const album = await sequelize.query(`DELETE FROM albums WHERE id = ${idAlbum}`, {type: sequelize.QueryTypes.DELETE})
        res.json({"message": "album eliminado"})
    }catch (error){
        console.log(error.message)
        res.status(400).json({"error": error.message})
    }
})


module.exports = router