const express = require('express')
const router = express.Router()
const { sequelize } = require("../connection/sequelize")

router.get("/", async (req, res) => {
    const songs = await sequelize.query("SELECT * FROM songs", {type: sequelize.QueryTypes.SELECT})
    res.json({songs})
})

router.get("/:id", async (req, res) => {
    const idSong = parseInt(req.params.id)
    try {
        const songs = await sequelize.query(`SELECT songs.*, bands.*, albums.* FROM songs INNER JOIN albums ON albums.album_id = songs.album INNER JOIN bands ON albums.band = bands.band_id WHERE songs.song_id = ${idSong}`, {type: sequelize.QueryTypes.SELECT})
        if (songs.length === 0) {
            res.status(400).json({"message": "The song does not exist"})
        } else{
            res.status(200).json({songs});    
        }
    } catch (error) {
        res.status(400).json({"error": error.message})
    }
})


router.post("/", async (req, res) => {
    const {song_name, duration, album, song_publication_date} = req.body
    const song = await sequelize.query(`INSERT INTO songs (song_name, duration, album, song_publication_date) VALUES ("${song_name}","${duration}","${album}","${song_publication_date}")`, {type: sequelize.QueryTypes.INSERT})
    res.json({song})
})

router.put("/:id", async (req, res) => {
    const {song_name, duration, album, song_publication_date} = req.body
    const idSong = parseInt(req.params.id)
    try{
        const song = await sequelize.query(`UPDATE songs SET song_name = "${song_name}", duration = "${duration}", album = "${duration}", album = "${album}", band = "${band}", song_publication_date = "${song_publication_date}" WHERE id = ${idSong}`, {type: sequelize.QueryTypes.UPDATE})
        res.json({song})
    }catch (error){
        console.log(error.message)
        res.status(400).json({"error": error.message})
    }
})

router.delete("/:id", async (req, res) => {
    const idSong = parseInt(req.params.id)
    try{
        const song = await sequelize.query(`DELETE FROM songs WHERE id = ${idSong}`, {type: sequelize.QueryTypes.DELETE})
        res.json({"message": "cancion eliminada"})
    }catch (error){
        console.log(error.message)
        res.status(400).json({"error": error.message})
    }
})


module.exports = router