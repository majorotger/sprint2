const express = require("express")
//const { sequelize } = require("./src/connection/sequelize")

const app = express()

const port = 3000

app.use(express.json())

const bands = require('./src/routes/bands')
app.use('/bands', bands)

const songs = require('./src/routes/songs')
app.use('/songs', songs)

const albums = require('./src/routes/albums')
app.use('/albums', albums)

app.listen(port, console.log(`Listening in ${port}`))