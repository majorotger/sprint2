 CREATE TABLE usuarios (id INT AUTO_INCREMENT, nombre varchar(100), apellido varchar (100), edad int(5), PRIMARY KEY (id));

  CREATE TABLE bandasMusicales (id INT AUTO_INCREMENT NOT NULL, nombre varchar(100) NOT NULL, integrantes INT NOT NULL, fecha_inicio year NOT NULL, fecha_separacion year NULL, pais varchar(100) NOT NULL ,PRIMARY KEY (id));

  CREATE TABLE canciones (id INT AUTO_INCREMENT NOT NULL, nombre varchar(100) NOT NULL, duracion INT NOT NULL, album INT NOT NULL, banda INT NULL, fecha_publicacion date NOT NULL ,PRIMARY KEY (id));

  CREATE TABLE albumes (id INT AUTO_INCREMENT NOT NULL, nombre_album varchar(100) NOT NULL, banda INT NOT NULL, fecha_publicacion date NOT NULL ,PRIMARY KEY (id));

  INSERT INTO bandasMusicales (nombre, integrantes, fecha_inicio, fecha_separacion, pais) VALUE ( 'The Beatles',4,'1960','1970','inglaterra' );

  INSERT INTO bandasMusicales (nombre, integrantes, fecha_inicio, fecha_separacion, pais) VALUE ( 'Sui Generis',2,'1969','1975','argentina' );

  INSERT INTO albumes (nombre_album, banda, fecha_publicacion) VALUE ( 'abbey road',1,'1969-09-26');

  INSERT INTO albumes (nombre_album, banda, fecha_publicacion) VALUE ( 'confesiones de invierno',2,'1973-08-01');

   INSERT INTO canciones (nombre, duracion, album, banda, fecha_publicacion) VALUE ( 'Here Comes the Sun',185,1,1,'1969-04-01');
   INSERT INTO canciones (nombre, duracion, album, banda, fecha_publicacion) VALUE ( 'Because',165,1,1,'1969-04-01');

   INSERT INTO canciones (nombre, duracion, album, banda, fecha_publicacion) VALUE ( 'Cuando ya me empiece a quedar solo',217,2,2,'1973-08-01');

   INSERT INTO canciones (nombre, duracion, album, banda, fecha_publicacion) VALUE ( 'Bienvenidos al tren',194,2,2,'1973-08-01');

   SELECT * FROM bandasMusicales;

   SELECT * FROM bandasMusicales WHERE pais = "argentina";

   SELECT * FROM bandasMusicales WHERE integrantes = 1;

   SELECT * FROM canciones WHERE fecha_publicacion > "2014-12-31";

   SELECT * FROM canciones WHERE duracion > 179;

   SELECT * FROM albumes;
