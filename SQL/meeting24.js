const express = require("express")
const { sequelize } = require("./src/connection/sequelize")
const { getTareas } = require("./src/repositories/tareasRepository")

const app = express()

const port = 3000

app.use(express.json())

/*app.get("/tareas/", async (req, res) => {
    const tareas = await sequelize.query("SELECT * FROM tareas", {type: sequelize.QueryTypes.SELECT})
    res.json({tareas})
    
})*/

app.get("/tareas/", async (req, res) => {
    const {terminadas} = req.query
    const tareas = await getTareas(terminadas)
    res.json({tareas})
})

app.post("/tareas/", async (req, res) => {
    const {titulo, descripcion} = req.body
    const tarea = await sequelize.query(`INSERT INTO tareas (titulo, descripcion) VALUES ("${titulo}","${descripcion}")`, {type: sequelize.QueryTypes.INSERT})
    res.json({tarea})
})

app.listen(port, console.log(`Listening in ${port}`))

