const { sequelize } = require("../connection/sequelize")


exports.getTareas = async (terminadas) => {
    let tareas
    if(terminadas == "true")
    {
        tareas = await sequelize.query("SELECT * FROM tareas WHERE terminada = true", {type: sequelize.QueryTypes.SELECT})    
    }else{
        tareas = await sequelize.query("SELECT * FROM tareas", {type: sequelize.QueryTypes.SELECT})
    }
    return tareas
}