const { sequelize } = require("../connection/sequelize")

sequelize.authenticate()
    .then(() => {
        console.log("Connection established")
    })