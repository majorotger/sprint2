const express = require("express")
const app = express()
const config = require("./config")
const Sequelize = require("sequelize")
const sequelize = new Sequelize(config.db, config.user, config.pass, {
    host: config.host,
    dialect: "mariadb"
})

async function test() {
    try{
        await sequelize.authenticate()
        console.log("connection has been established successfully")
    } catch (error) {
        console.error("unable to connect to the database:", error.message)
    }
}

test()

sequelize.query("SELECT * FROM bandasMusicales", {
    type: sequelize.QueryTypes.SELECT
})
.then(function (result){
    console.log(result)
})

app.listen(config.node_port, console.log(`Listening in ${config.node_port}`))