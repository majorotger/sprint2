require('dotenv').config();

const config = {
    port: process.env.NODE_PORT || 3000,
    apikey: process.env.API_KEY
}

module.exports = config;
