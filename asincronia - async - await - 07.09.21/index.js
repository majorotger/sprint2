const config = require('./config');
const express = require('express');
const app = express();


const climaController = require('./climaController');


app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));


app.get('/clima', climaController);



//................................ Puerto ...............................
app.listen(config.port, () => console.log(`Escuchando puerto: ${config.port}`))
