const express = require("express")
const app = express()
const citys = require("./data")
const axios = require('axios')

app.use(express.json())

/*function randomCityName(items) {
    return items[Math.floor((Math.random()*items.length))];
}*/

function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}


async function tempsCity(city) {
    try {
        const obtenerDatos = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city.name}&appid=87bae2ffee43eec860796931e362d3ad&units=metric`)
        const temp = obtenerDatos.data.main.temp;
        return temp;
    }
    catch (error) {
        throw Error(error);
    }
};


app.get('/clima', async (req, res) => {

    let randomCity = getRandom(citys, 3);
    const finalList = [];
    for (city of randomCity) {
        try {
            const temp = await tempsCity(city);
            const item = { city, temp };
            console.log(`Datos recibidos:`, item);
            finalList.push(item)
        } catch (error) {
            res.status(500).json({ msg: "Error al obtener los datos del clima" });
            return
        }
    }
    res.status(200).json(finalList);
});








app.listen(3000, console.log("Escuchando en el puerto 3000"))