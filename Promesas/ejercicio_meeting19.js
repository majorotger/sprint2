/*const axios = require ("axios")
const express = require('express')
const cities = require("./data")
const app = express()

//const arrayCiudades = ["RG", "USH", "TOL", "Rio_Gallegos", "Bariloche", "Calafate", "Cipolleti", "Comodoro", "Trelew", "Epuyen"]

app.get('/cities', function (req, res) {
    const newArray = cities.slice(0,3)
    //res.status(200).json(newArray)

    axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArray[0].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
        .then((response) => {
            return response.data;
        })
        .then((data) => {
            return data.main;
        })
        .then((temp) => {
            newArray[0].temperature = temp.temp;
            axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArray[1].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
                .then((response) => {
                    return response.data;
                })
                .then((data) => {
                    return data.main;
                })
                .then((temp) => {
                    newArray[1].temperature = temp.temp;
                    axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArray[2].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
                        .then((response) => {
                            return response.data;
                        })
                        .then((data) => {
                            return data.main;
                        })
                        .then((temp) => {
                            newArray[2].temperature = temp.temp;
                            res.status(200).json({"response": newArray});
                        })
                })
        })
        .catch((error) => {
            console.log(error);
            reject("ERROR");
        })

})

app.listen(3000, function () {
    console.log(`Escuchando el puerto 3000`);
})*/


const express = require("express");
const app = express();
const citys = require("./data");
const axios = require('axios');

app.use(express.json());

function randomCityName(items) {
    return items[Math.floor((Math.random()*items.length))];
}

app.get('/clima', (req, res) => {
    let newArrayCitys = [];
    const eliminarRepetidos = arr => [...new Set(arr)];
    while (newArrayCitys.length < 3) {
        let randomCity = randomCityName(citys);
        newArrayCitys.push(randomCity);
        newArrayCitys = eliminarRepetidos(newArrayCitys);
    };
    axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[0].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
        .then((respuesta) => {
            newArrayCitys[0].temperatura = respuesta.data.main.temp;
            axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[1].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
                .then((respuesta) => {
                    newArrayCitys[1].temperatura = respuesta.data.main.temp;
                    axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[2].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
                        .then((respuesta) => {
                            newArrayCitys[2].temperatura = respuesta.data.main.temp;
                            res.status(200).json({"response": newArrayCitys});
                        })
                })
        })
        .catch((error) => {
            console.log(error);
            reject("ERROR");
        })
        
        
});

app.listen(3000, console.log("Escuchando en el puerto 3000"))
