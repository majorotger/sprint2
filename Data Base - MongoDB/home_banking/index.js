const express = require('express')
const app = express()
const config = require('./config')


app.use(express.json())

const users = require('./routes/users')
app.use('/users', users)

app.listen(config.port, console.log(`Escuchando en el puerto ${config.port}`))