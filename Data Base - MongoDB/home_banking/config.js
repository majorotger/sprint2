require('dotenv').config();

const config = {
    port: process.env.NODE_PORT || 3000,
    mongoDB: process.env.MONGODB_URL || 'mongodb://localhost:27017'
}

module.exports = config;