const mongoose = require('mongoose')
const config = require('./config')

mongoose.connect(`${config.mongoDB}/mrdb`)

module.exports = mongoose