const mongoose = require('../connection')

/*const ContactPoint = mongoose.model("contacts", {
    contactPoint: String
})*/

const User = mongoose.model("users", {
    first_name: String,
    last_name: String,
    email: String,
    balance: Number,
    //contactPoint: [ContactPoint]
})

module.exports = User