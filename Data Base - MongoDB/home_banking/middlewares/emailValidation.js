const Users = require("../models/users");

async function emailValidation (req, res, next) {
    try {
        const findUser = await Users.findOne({email: req.body.email})
        if (findUser) {
            next();
        } else {
            res.status(400).json({"message": "The user does not exist"})
    }
    } catch (error) {
        res.status(422).json({"message": "error!"})
    }
}

module.exports = emailValidation