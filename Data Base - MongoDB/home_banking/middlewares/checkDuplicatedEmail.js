const User = require("../models/users")

async function checkDuplicatedEmail(req, res, next) {
    try {
            const findEmail = await User.findOne({email: req.body.email})
            if (!findEmail) {
                next();
            } else {
                res.status(400).json({"message": "email already exists"})
            }
    } catch (error) {res.status(422).json({"response": "error!"})}
}

module.exports = checkDuplicatedEmail