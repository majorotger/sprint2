const express = require('express')
const app = express()
const router = express.Router()
const User = require('../models/users')
const checkDuplicatedEmail = require('../middlewares/checkDuplicatedEmail')
const emailValidation = require('../middlewares/emailValidation')

app.use(express.json())

router.post('/', checkDuplicatedEmail,async (req, res) => {
    const {first_name, last_name, email, balance, contacPoint} = req.body
    const newUser = new User({
        first_name: first_name,
        last_name: last_name,
        email: email,
        balance: balance
        //contacPoint: contacPoint
    }) 
    const user = await newUser.save();
    res.status(201).json({"message": "User created successfully"})
})

router.put('/', emailValidation, async (req, res) => {
    const doc = await User.findOne({email: req.body.email})
    const newBalance = doc.balance + req.body.amount
    doc.balance = newBalance
    await doc.save()
    res.status(200).json({"message": "Balance updated successfully"})
})


router.put('/transfer', emailValidation, async (req, res) => {
    const session = await User.startSession()
    try {
        session.startTransaction()
        const sender = await User.findOne({email: req.body.email})
        const receiver = await User.findOne({email: req.body.emailReceiver})
        if(receiver){ 
            if(sender.balance >= req.body.transferAmount){
                const newReceiverBalance = receiver.balance + parseInt(req.body.transferAmount)
                const newSenderBalance = sender.balance - parseInt(req.body.transferAmount)
                receiver.balance = newReceiverBalance
                sender.balance = newSenderBalance
                await sender.save()
                await receiver.save()
                await session.commitTransaction()
                res.status(200).json({"message": "The transfer was successful"})
            }
            else{res.status(400).json({"message": "Your balance is not enough"})}
        }
        else{res.status(400).json({"message": "The receiver user does not exist"})}
    } catch (error) {
        await session.abortTransaction()
        res.status(422).json({"message": "error!"})
    }
    session.endSession()
})

module.exports = router

